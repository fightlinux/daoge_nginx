--[[
Author: 方文道-技术运维部 fangwd@daqsoft.com
Date: 2022-07-12 12:11:02
LastEditors: 方文道-技术运维部 fangwd@daqsoft.com
LastEditTime: 2022-07-12 12:11:02
FilePath: /opsany_waf/lua/opsany_waf/config.lua
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
--]]
--WAF config file,enable = "on",disable = "off"

--waf status
config_waf_enable = "on"
--log dir
config_log_dir = "/usr/local/openresty/nginx/waf_logs"
--rule setting
config_rule_dir = "/usr/local/openresty/nginx/lua/opsany_waf/rule-config"
--enable/disable white url
config_white_url_check = "on"
--enable/disable white ip
config_white_ip_check = "on"
--enable/disable block ip
config_black_ip_check = "on"
--enable/disable url filtering
config_url_check = "on"
--enalbe/disable url args filtering
config_url_args_check = "on"
--enable/disable user agent filtering
config_user_agent_check = "on"
--enable/disable cookie deny filtering
config_cookie_check = "on"
--enable/disable cc filtering
config_cc_check = "on"
--cc rate the xxx of xxx seconds
config_cc_rate = "100/30"
--enable/disable post filtering
config_post_check = "on"
--config waf output redirect/html
config_waf_output = "html"
--if config_waf_output ,setting url, https://www.unixhot.com
config_waf_redirect_url = "/"
config_output_html=[[
	<!DOCTYPE html>
	<html style="height:100%;width:100%">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta http-equiv="Server" content="CloudWAF">
			<title id="title">访问被拦截！</title>
		</head>
		<body onload="bindall()" style="height:100%;width:100%;margin:0;font-family:Microsoft yahei">
			<div style="min-height:13.125rem;height:17%;width:100%">
				<div style="margin-top:3.75rem;margin-left:18.4375rem;height:52.38;width:83.04%">
					<svg xmlns="http://www.w3.org/2000/svg" style="width:3.125rem;height:3.125rem" viewBox="0 0 50 50">
						<path fill="#e84e4c" d="M25,0A25,25,0,1,0,50,25,25,25,0,0,0,25,0Zm1.6,37.16H22.85V33.41H26.6Zm0-6.63H22.85L22.35,13H27.1Z"/>
					</svg>
					<font style="font-family:MicrosoftYaHei;font-size:4.375rem;color:#e94d4c;margin-left:.75rem;font-weight:700">418</font>
				</div>
				<div style="margin-left:18.4375rem;height:47.62%;width:83.04%">
					<font id="a" style="font-family:MicrosoftYaHei;font-size:1.875rem;color:#999;word-wrap:break-word">您的请求疑似攻击行为！</font>
					<p style="font-family:MicrosoftYaHei;font-size:.9rem;color:#999;word-wrap:break-word"></p>
				</div>
			</div>
			<div style="height:15.625rem;width:100%;min-width:105rem">
				<div style="margin-left:18.4375rem;float:left;width:50rem">
					<p id="b" style="margin-top:10px">如果您的访问路径合法，请联系站长修改策略，让您的访问不再被拦截</p>
				</div>
			</div>
		</body>
	</html>
]]

