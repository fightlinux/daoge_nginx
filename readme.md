# daoge_nginx

- 使用Nginx+Lua实现自定义WAF（Web application firewall） 
- 依据赵班长编写的WAF进行修改(https://gitee.com/unixhot/waf.git)

## 项目背景介绍

### 需求产生

为了快速进行Nginx代理部署，并增加Nginx安全防护功能，集成了赵班长开源的WAF代码。

### Nginx功能列表

1. 支持自定义错误页面，在配置文件中添加对应调用即可；
2. 支持URL黑名单功能，访问黑名单中的地址直接返回404，可能与WAF中的配置冲突；
3. 添加默认站点目录，html/default
4. 添加跨域白名单配置

### WAF实现

WAF一句话描述，就是解析HTTP请求（协议解析模块），规则检测（规则模块），做不同的防御动作（动作模块），并将防御过程（日志模块）记录下来。所以本文中的WAF的实现由五个模块(配置模块、协议解析模块、规则模块、动作模块、错误处理模块）组成。

### WAF功能列表

1.	支持IP白名单和黑名单功能，直接将黑名单的IP访问拒绝。
2.	支持URL白名单，将不需要过滤的URL进行定义。
3.	支持User-Agent的过滤，匹配自定义规则中的条目，然后进行处理（返回403）。
4.	支持CC攻击防护，单个URL指定时间的访问次数，超过设定值，直接返回403。
5.	支持Cookie过滤，匹配自定义规则中的条目，然后进行处理（返回403）。
6.	支持URL过滤，匹配自定义规则中的条目，如果用户请求的URL包含这些，返回403。
7.	支持URL参数过滤，原理同上。
8.	支持日志记录，将所有拒绝的操作，记录到日志中去。
9.	日志记录为JSON格式，便于日志分析，例如使用ELK进行攻击日志收集、存储、搜索和展示。

## 安装部署

使用docker-compose + openresty方式进行部署

### 环境安装

**需要安装docker 与 docker-compose，在此不进行详细说明**

### 项目部署

```
cd /opt
git clone https://gitee.com/daogef/daoge_nginx.git
cd daoge_nginx
chmod 777 waf_logs
docker-compose run --rm nginx nginx -t  &&  sleep 3 && docker-compose up -d nginx

```

## 项目使用

### 跨域白名单添加

在conf.d/default.conf文件中找到如下配置内容，在其中添加白名单即可

```
# 跨域白名单配置
map $http_origin $allow_origin {
  default "";
  "~^(https?://localhost(:[0-9]+)?)" $1;
  "~^(https?://127.0.0.1(:[0-9]+)?)" $1;
}
```

例如需要添加允许*.baidu.com进行跨域调用，配置如下

```
# 跨域白名单配置
map $http_origin $allow_origin {
  default "";
  "~^(https?://localhost(:[0-9]+)?)" $1;
  "~^(https?://127.0.0.1(:[0-9]+)?)" $1;
  "~^(https?://.*.baidu.com)" $1;
}
```



